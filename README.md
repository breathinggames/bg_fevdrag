# DragBreath

DragBreath is a drag racing video game intended to gamify the diagnosis of obstructive and restrictive lung disease by calculating the Tiffeneau-Pinelli index, also known as the FEV1/FVC ratio.
